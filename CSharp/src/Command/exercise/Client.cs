﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Command.Exercise
{
    public class Client<T>
    {
        private T checkCommand;
        private Result result = new Result(Status.PENDING);

        public Client(T command)
        {
            this.checkCommand = command;
        }
        public void setResult(Result result)
        {
            this.result = result;
        }
        public Result getResult()
        {
            return result;
        }
        public void setCheckCommand(T command)
        {
            this.checkCommand = command;
        }
        public T getCheckCommand()
        {
            return checkCommand;
        }
    }
}
